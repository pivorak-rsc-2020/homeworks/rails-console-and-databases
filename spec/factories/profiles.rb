FactoryBot.define do
  factory :profile do
    bio        { Faker::Movies::HitchhikersGuideToTheGalaxy.quote }
    first_name { Faker::Movies::HitchhikersGuideToTheGalaxy.character }
    last_name  { Faker::Movies::HitchhikersGuideToTheGalaxy.planet }
    age        { Faker::Number.between(from: 18, to: 100) }

    user
  end
end
